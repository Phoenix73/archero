﻿
using System;
using UniRx;

public static class GameReport
{
    public enum GameState
    {
        PreStart,
        Playing,
        GameOver,
        Win
    }


    private static ReactiveProperty<GameState> _gameStateProperty = new ReactiveProperty<GameState>(GameState.PreStart);
    
    public static GameState CurrentState
    {
        get => _gameStateProperty.Value;
        set => _gameStateProperty.Value = value;
    }

    public static IObservable<GameState> OnChangeGameState => _gameStateProperty;

    public static bool IsPlaying => CurrentState == GameState.Playing;
}
