﻿using Configs;
using Model;
using UI;
using UnityEngine;

namespace Game
{
    public class GameBehaviour : MonoBehaviour
    {
        [SerializeField] private Transform _rootForMap;
        [SerializeField] private MoveControl _moveControl;
        
        public IMapBehaviour Map { get; private set; }

        public IPlayerBehaviour PlayerComponent { get; private set; }
        
        public void CreateLevel(GameModel model)
        {
            CreateLevel(model.CurrentLevel);
            
            PlayerComponent = Map.SpawnPlayer(model.Player);
            PlayerComponent.MoveControl = _moveControl;
            
            Map.SpawnEnemy(model.CurrentLevel.UsedEnemyStorage);
        }

        private void CreateLevel(ILevelConfig levelConfig)
        {
            CreateMap(levelConfig.MapConfig);
        }

        private void CreateMap(IMapConfig mapConfig)
        {
            GameObject mapObject = Instantiate(mapConfig.GetPrefabMap, _rootForMap, false);
            Map = mapObject.GetComponent<IMapBehaviour>();
        }
    }
}

