﻿using Model;

namespace Game
{
    public interface IPlayerCollision
    {
        void Init(IPlayer player);
    }
}

