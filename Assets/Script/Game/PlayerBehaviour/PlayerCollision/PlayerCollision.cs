﻿using System;
using Model;
using UniRx;
using UnityEngine;
using Utils;
using View;

namespace Game
{
    public class PlayerCollision : MonoBehaviour, IPlayerCollision
    {
        private IPlayer _player;
        
        public void Init(IPlayer player)
        {
            _player = player;
            GameRoot.Instance.GameModel.DamageManager.OnKilled.Subscribe(OnKilled).AddTo(this);
        }
        
        private void OnCollisionEnter(Collision other)
        {
            TriggerEnemy(other.transform);
        }

        private void OnTriggerEnter(Collider other)
        {
            TriggerShellDamage(other.transform);
        }

        private void TriggerShellDamage(Transform triggeredObject)
        {
            if (triggeredObject.CompareTag(TagStorage.TagShell))
            {
                var shell = triggeredObject.GetComponent<Shell>();
                GameRoot.Instance.GameModel.DamageManager.SetDamage(_player, shell.Damage);

                shell.Dispose();
            }
        }

        private void TriggerEnemy(Transform triggeredObject)
        {
            if (triggeredObject.CompareTag(TagStorage.TagEnemy))
            {
                var enemy = triggeredObject.GetComponent<IEnemyBehaviour>();
                GameRoot.Instance.GameModel.DamageManager.SetDamage(_player, enemy.EnemyData.EnemyConfig.DamageFromCollision);
            }
        }

        private void OnKilled(string id)
        {
            if (id == _player.Id)
            {
                GameReport.CurrentState = GameReport.GameState.GameOver;
                NavigationView.Instance.PushView<GameOverView>();
            }
        }
    }
}

