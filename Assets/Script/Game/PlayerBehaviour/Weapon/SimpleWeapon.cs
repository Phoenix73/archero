﻿using UnityEngine;

namespace Game
{
    public class SimpleWeapon : WeaponBehaviour
    {
        [SerializeField] private SmartAim _aim;
        [SerializeField] private LineShell _prefabShell;

        [SerializeField] private bool _isUseDetectTarget;
        
        private Transform _currentTarget;
        
        public override void TryShoot()
        {
            var result = _aim.GetTarget;

            if (!result.isSuccess)
            {
                return;
            }
            
            _currentTarget = result.target;

            if (_isUseDetectTarget)
            {
                Vector3 target = _currentTarget.position;
                _owner.transform.LookAt(target);
            }


            base.TryShoot();
        }

        protected override void Shoot()
        {
            Transform rootShell = GameRoot.Instance.GameBehaviour.Map.CachedTransform;
            
            LineShell shell = Instantiate(_prefabShell, rootShell, false);
            
            shell.transform.position = _spawnPlace.position;
            shell.Init(_weaponConfig.Damage, _weaponConfig.SpeedShell, _currentTarget.position);
            shell.Play();
        }
    }
}

