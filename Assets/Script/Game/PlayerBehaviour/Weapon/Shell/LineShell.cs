﻿using UnityEngine;

namespace Game
{
    public class LineShell : Shell
    {
        [SerializeField] private Rigidbody _mover;
        
        public Vector3 Direction { get; set; }

        private float _maxDistanceFromSpawn = 15f;

        public override void Init(int damage, float speed, Vector3 target)
        {
            base.Init(damage, speed, target);
            Direction = target - CachedTransform.position;
            Direction = Direction.normalized;
        }

        protected override void Move()
        {
            _mover.velocity = _speed * Direction;
        }
    }
}

