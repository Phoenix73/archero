﻿using UnityEngine;

namespace Game
{
    public interface IShell
    {
        int Damage { get; }

        void Init(int damage, float speed, Vector3 target);

        void Dispose();

        Transform CachedTransform { get; }

        void Play();
    }
}

