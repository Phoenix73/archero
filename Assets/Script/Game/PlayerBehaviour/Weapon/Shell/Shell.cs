﻿using UnityEngine;
using Utils;

namespace Game
{
    public abstract class Shell : MonoBehaviour, IShell
    {
        public int Damage { get; private set; }

        protected float _speed;
        protected bool _isPlay;

        protected Vector3 _target;

        public virtual void Init(int damage, float speed, Vector3 target)
        {
            Damage = damage;
            _speed = speed;
            _target = target;

            CachedTransform = transform;
        }

        public void Play()
        {
            _isPlay = true;
        }

        public void Dispose()
        {
            Destroy(gameObject);
        }

        public Transform CachedTransform { get; private set; }

        protected abstract void Move();

        private void Update()
        {
            if (_isPlay && GameReport.IsPlaying)
            {
                Move();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.transform.CompareTag(TagStorage.TagWall))
            {
                Dispose();
            }
        }
    }
}

