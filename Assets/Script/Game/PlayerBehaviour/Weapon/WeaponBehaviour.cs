﻿using Configs;
using UnityEngine;

namespace Game
{
    public abstract class WeaponBehaviour : MonoBehaviour
    {
        [SerializeField] protected Transform _spawnPlace;
        
        protected IWeaponConfig _weaponConfig;

        private float _currentSecondsReload;
        protected Transform _owner;
        
        public void Init(IWeaponConfig weaponConfig, Transform owner)
        {
            _owner = owner;
            _weaponConfig = weaponConfig;
        }

        public virtual void TryShoot()
        {
            if (_currentSecondsReload <= 0)
            {
                Shoot();
                Reload();
            }
            else
            {
                Reload();
            }
        }

        private void Reload()
        {
            _currentSecondsReload += Time.deltaTime;

            if (_currentSecondsReload > _weaponConfig.SecondsReload)
            {
                _currentSecondsReload = 0;
            }
        }

        protected abstract void Shoot();
    }
}

