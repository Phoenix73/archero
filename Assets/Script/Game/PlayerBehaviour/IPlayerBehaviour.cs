﻿using Model;
using UI;
using UnityEngine;

namespace Game
{
    public interface IPlayerBehaviour
    {
        void Init(IPlayer player);
        
        IMoveControl MoveControl { get; set; }
        
        IPlayer Player { get; }
        
        Transform CachedTransform { get; }
        
        Transform TargetShoot { get; }
    }
}

