﻿using Model;
using UnityEngine;

namespace Game
{
    public class PlayerSpawn : MonoBehaviour, IPlayerSpawn
    {
        [SerializeField] private Transform[] _cellsForSpawn;
        [SerializeField] [Space(5)] private Transform _rootForPlayer;

        public IPlayerBehaviour SpawnPlayer(IPlayer player)
        {
            int indexCell = UnityEngine.Random.Range(0, _cellsForSpawn.Length);

            Transform cellForSpawn = _cellsForSpawn[indexCell];
            GameObject gameObjectPlayer = Instantiate(player.PlayerConfig.PlayerPrefab, _rootForPlayer, false);
            gameObjectPlayer.transform.position = cellForSpawn.position;
            
            IPlayerBehaviour playerBehaviour = gameObjectPlayer.GetComponent<IPlayerBehaviour>();
            playerBehaviour.Init(player);


            return playerBehaviour;
        }
    }
}

