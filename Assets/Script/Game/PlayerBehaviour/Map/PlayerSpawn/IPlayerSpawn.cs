﻿using Model;

namespace Game
{
    public interface IPlayerSpawn
    {
        IPlayerBehaviour SpawnPlayer(IPlayer player);
    }
}

