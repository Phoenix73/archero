﻿using System.Collections.Generic;
using Configs;
using Model;
using UnityEngine;

namespace Game
{
    public class MapBehaviour : MonoBehaviour, IMapBehaviour
    {
        [SerializeField] private PlayerSpawn _playerSpawn;
        [SerializeField] private EnemySpawn _enemySpawn;
        
        public Dictionary<string, IEnemyBehaviour> AvailableEnemies { get; private set; }

        private void Awake()
        {
            CachedTransform = transform;
        }

        public void RemoveEnemy(string id)
        {
            if (AvailableEnemies.ContainsKey(id))
            {
                AvailableEnemies.Remove(id);
            }
        }

        public Transform CachedTransform { get; private set; }

        public IPlayerBehaviour SpawnPlayer(IPlayer player)
        {
           return _playerSpawn.SpawnPlayer(player);
        }

        public void SpawnEnemy(IEnemyStorageConfig enemyStorageConfig)
        {
            AvailableEnemies = _enemySpawn.SpawnEnemies(enemyStorageConfig);
        }
    }
}

