﻿using System.Collections.Generic;
using Configs;
using Model;
using UnityEngine;

namespace Game
{
    public interface IMapBehaviour
    {
        IPlayerBehaviour SpawnPlayer(IPlayer player);
        void SpawnEnemy(IEnemyStorageConfig enemyStorageConfig);
        Dictionary<string, IEnemyBehaviour> AvailableEnemies { get; }

        void RemoveEnemy(string id);
        
        Transform CachedTransform { get; }
    }
}

