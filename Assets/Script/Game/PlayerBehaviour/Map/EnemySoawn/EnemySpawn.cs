﻿using System.Collections.Generic;
using Configs;
using Model;
using UnityEngine;
using Utils;

namespace Game
{
    public class EnemySpawn : MonoBehaviour
    {
        [SerializeField] private Transform[] _cellsForSpawn;
        [SerializeField] [Space(5)] private Transform _rootForEnemies;

        private IGeneratorIntID _generatorId;

        public Dictionary<string, IEnemyBehaviour> SpawnEnemies(IEnemyStorageConfig storageConfig)
        {
            Dictionary<string, IEnemyBehaviour> enemies = new Dictionary<string, IEnemyBehaviour>();
            _generatorId = new GeneratorIntID();
            List<Vector3> availableSpawnPoints = GetAvailableSpawnPoint();

            foreach (var enemyConfig in storageConfig.GetEnemies)
            {
                int randomIndex = UnityEngine.Random.Range(0, availableSpawnPoints.Count);
                Vector3 spawnPosition = availableSpawnPoints[randomIndex];
                availableSpawnPoints.Remove(spawnPosition);

               IEnemyBehaviour enemyBehaviour = CreateEnemy(enemyConfig, _generatorId.GetId.ToString(), spawnPosition);
               enemies.Add(enemyBehaviour.Id, enemyBehaviour);
            }

            return enemies;
        }

        private List<Vector3> GetAvailableSpawnPoint()
        {
            List<Vector3> availableSpawnPoints = new List<Vector3>();

            for (int i = 0; i < _cellsForSpawn.Length; i++)
            {
                availableSpawnPoints.Add(_cellsForSpawn[i].position);
            }

            return availableSpawnPoints;
        }

        private IEnemyBehaviour CreateEnemy(IEnemyConfig enemyConfig, string id, Vector3 position)
        {
            IEnemy enemy = new Enemy(enemyConfig, id);
            
            GameObject gameObjectEnemy = Instantiate(enemyConfig.EnemyPrefab, _rootForEnemies, false);
            IEnemyBehaviour enemyBehaviour = gameObjectEnemy.GetComponent<IEnemyBehaviour>();
            
            enemyBehaviour.Init(enemy);
            enemyBehaviour.CachedTransform.position = position;
            
            enemyBehaviour.MoveEnemy = new LineMoveEnemies();
            enemyBehaviour.MoveEnemy.Init(enemyConfig, position, enemyBehaviour.Rigidbody);

            return enemyBehaviour;
        }
    }
}

