﻿using Configs;
using Model;
using UI;
using UnityEngine;

namespace Game
{
    public sealed class PlayerBehaviour : MonoBehaviour, IPlayerBehaviour
    {
        [SerializeField] private Rigidbody _mover;
        [SerializeField] private PlayerCollision _playerCollision;
        [SerializeField] private Transform _targetShoot;
        
        private Vector3 _stateIdle = Vector3.zero;
        
        private Transform _cachedTransform;
        private WeaponBehaviour _weapon;

        public IMoveControl MoveControl { get; set; }
        
        public IPlayer Player { get; private set; }
        public Transform CachedTransform => _cachedTransform;
        public Transform TargetShoot => _targetShoot;

        public void Init(IPlayer player)
        {
            Player = player;
            _playerCollision.Init(player);

            _cachedTransform = transform;

            CreateWeapon(player.PlayerConfig.WeaponConfig);
        }
        
        private void Update()
        {
            if (GameReport.IsPlaying)
            {
                GetMoveData();
            }
        }

        private void GetMoveData()
        {
            Vector3 direction = MoveControl.GetDirection;

            if (direction.Equals(_stateIdle))
            {
                Stop();
            }
            else
            {
                _cachedTransform.rotation = Quaternion.LookRotation(direction);
                Move(direction);
            } 
        }
        
        private void Move(Vector3 direction)
        {
            _mover.MovePosition(_cachedTransform.position + direction * Player.PlayerConfig.Speed * Time.deltaTime);
        }

        private void Stop()
        {
            _mover.velocity = _stateIdle;
            _weapon.TryShoot();
        }
        
        private void CreateWeapon(IWeaponConfig weaponConfig)
        {
            GameObject weaponGameObject = Instantiate(weaponConfig.PrefabWeapon, CachedTransform, false);
            _weapon = weaponGameObject.GetComponent<WeaponBehaviour>();  
            _weapon.Init(weaponConfig, CachedTransform);
        }
    }
}

