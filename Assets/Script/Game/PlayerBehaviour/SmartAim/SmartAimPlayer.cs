﻿using UnityEngine;

namespace Game
{
    public class SmartAimPlayer : SmartAim
    {
        public (bool isSuccess, Transform target) GetTarget => FindTarget();

        protected override (bool isSuccess, Transform target) FindTarget()
        {
            var enemies = GameRoot.Instance.GameBehaviour.Map.AvailableEnemies;
            var player = GameRoot.Instance.GameBehaviour.PlayerComponent;

            float distanceForEnemies = float.MaxValue;
            string idEnemy = string.Empty;

            foreach (var enemy in enemies.Values)
            {
                float checkedDistance = Vector3.Distance(player.CachedTransform.position, enemy.CachedTransform.position);

                if (checkedDistance < distanceForEnemies)
                {
                    distanceForEnemies = checkedDistance;
                    idEnemy = enemy.Id;
                }
            }

            if (string.IsNullOrEmpty(idEnemy))
            {
                return (false, null);
            }
            else
            {
                return (true, enemies[idEnemy].TargetShoot);
            }
        }
    }
}

