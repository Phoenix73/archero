﻿using UnityEngine;

namespace Game
{
    public abstract class SmartAim : MonoBehaviour
    {
        public (bool isSuccess, Transform target) GetTarget => FindTarget();

        protected abstract (bool isSuccess, Transform target) FindTarget();
    }
}

