﻿using UnityEngine;

namespace Game
{
    public class SmartAimEnemy : SmartAim
    {
        
        protected override (bool isSuccess, Transform target) FindTarget()
        {
            var player = GameRoot.Instance.GameBehaviour.PlayerComponent;

            if (player != null)
            {
                return (true, player.TargetShoot);
            }

            return (false, null);
        }
    }
}

