﻿using Configs;
using UnityEngine;

namespace Game
{
    public interface IMoveEnemy
    {
        void Init(IEnemyConfig enemyConfig, Vector3 spawnPoint, Rigidbody mover);

        void UpdateStateMove(float deltaTime);

        void ChangeDirectionMove();
        
        bool IsIdle { get; }

        void Stop();
    }
}

