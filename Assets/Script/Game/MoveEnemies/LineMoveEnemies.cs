﻿using Configs;
using UnityEngine;
using Utils;

namespace Game
{
    public class LineMoveEnemies : IMoveEnemy
    {
        private float _speed;
        private float _timeFreeze;
        private float _maxDistanceFromSpawn;

        private Vector3 _spawnPoint;
        private float _currentTimeFreeze = 0;

        private bool _isIdle = false;
        private Rigidbody _mover;

        private Vector3 _directionMove = Vector3.right;

        private Vector3 _minPoint;
        private Vector3 _maxPoint;

        public bool IsIdle => _isIdle;

        public void Init(IEnemyConfig enemyConfig, Vector3 spawnPoint, Rigidbody mover)
        {
            _mover = mover;
            _spawnPoint = spawnPoint;
            
            _speed = enemyConfig.Speed;
            _timeFreeze = enemyConfig.TimeFreeze;
            _maxDistanceFromSpawn = enemyConfig.MaxDistanceFromSpawn;

            _minPoint = _spawnPoint + Vector3.left * _maxDistanceFromSpawn;
            _maxPoint = spawnPoint + Vector3.right * _maxDistanceFromSpawn;
        }

        public void UpdateStateMove(float deltaTime)
        {
            if (_isIdle)
            {
                Stop(deltaTime);
            }
            else
            {
                Move(deltaTime);
            }
        }

        private void Move(float deltaTime)
        {
            Vector3 newPosition = _mover.transform.position + _speed * deltaTime * _directionMove;
            newPosition = ClampUtils.ClampVector(newPosition, _minPoint, _maxPoint);
            _mover.MovePosition(newPosition);

            if (newPosition == _minPoint || newPosition == _maxPoint)
            {
                _isIdle = true;
            }
        }

        private void Stop(float deltaTime)
        {
            Stop();
            _currentTimeFreeze += deltaTime;
            
            if (_currentTimeFreeze > _timeFreeze)
            {
                _currentTimeFreeze = 0;
                ChangeDirectionMove();
                _isIdle = false;
            }
        }

        public void ChangeDirectionMove()
        {
            if (_directionMove.x > 0)
            {
                _directionMove = Vector3.left;;
            }
            else
            {
                _directionMove = Vector3.right;
            }
        }

        public void Stop()
        {
            _mover.velocity = Vector3.zero; 
        }
    }
}

