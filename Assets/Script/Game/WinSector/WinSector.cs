﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using View;

namespace Game
{
    public class WinSector : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(TagStorage.TagPlayer))
            {
                GameReport.CurrentState = GameReport.GameState.Win;

                NavigationView.Instance.PushView<WinView>();
            }
        }
    }
}

