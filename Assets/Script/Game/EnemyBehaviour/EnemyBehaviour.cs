﻿using Configs;
using Model;
using UnityEngine;

namespace Game
{
    public class EnemyBehaviour : MonoBehaviour, IEnemyBehaviour
    {
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private EnemyCollision _enemyCollision;
        [SerializeField] private Transform _targetShoot;
        
        private WeaponBehaviour _weapon;
        
        public string Id => EnemyData.Id;
        public IEnemy EnemyData { get; private set; }

        public void Init(IEnemy enemyData)
        {
            EnemyData = enemyData;
            CachedTransform = transform;
            
            _enemyCollision.Init(this);

            CreateWeapon(enemyData.EnemyConfig.WeaponConfig);
        }

        public Transform CachedTransform { get; private set; }
        
        public void DestroyEnemy()
        {
            GameRoot.Instance.GameBehaviour.Map.RemoveEnemy(Id);
            Destroy(gameObject);
        }

        public IMoveEnemy MoveEnemy { get; set; }
        public Rigidbody Rigidbody => _rigidbody;

        public Transform TargetShoot => _targetShoot;

        private void Update()
        {
            if (GameReport.IsPlaying)
            {
                MoveEnemy.UpdateStateMove(Time.deltaTime);

                if (MoveEnemy.IsIdle)
                {
                    _weapon.TryShoot();
                }
            }
        }

        private void CreateWeapon(IWeaponConfig weaponConfig)
        {
            GameObject weaponGameObject = Instantiate(weaponConfig.PrefabWeapon, CachedTransform, false);
            _weapon = weaponGameObject.GetComponent<WeaponBehaviour>();  
            _weapon.Init(weaponConfig, CachedTransform);
        }
    }
}

