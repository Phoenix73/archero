﻿using System;
using UnityEngine;
using Utils;
using UniRx;

namespace Game
{
    public class EnemyCollision : MonoBehaviour
    {
        private IEnemyBehaviour _enemyBehaviour;
        
        public void Init(IEnemyBehaviour enemyBehaviour)
        {
            _enemyBehaviour = enemyBehaviour;
            GameRoot.Instance.GameModel.DamageManager.OnKilled.Subscribe(OnKilled).AddTo(this);
        }
        
        private void OnCollisionEnter(Collision other)
        {
            TriggeredWall(other.transform);
        }

        private void OnTriggerEnter(Collider other)
        {
            TriggeredShell(other.transform);
        }

        private void TriggeredShell(Transform triggeredObject)
        {
            if (triggeredObject.CompareTag(TagStorage.TagShell))
            {
                var shell = triggeredObject.GetComponent<IShell>();
                GameRoot.Instance.GameModel.DamageManager.SetDamage(_enemyBehaviour.EnemyData, shell.Damage);
                
                shell.Dispose();
            }
        }

        private void TriggeredWall(Transform triggeredObject)
        {
            if (triggeredObject.CompareTag(TagStorage.TagWall))
            {
                _enemyBehaviour.MoveEnemy.Stop();
                _enemyBehaviour.MoveEnemy.ChangeDirectionMove();
            }
        }
        
        private void OnKilled(string id)
        {
            if (id == _enemyBehaviour.Id)
            {
                GameRoot.Instance.GameModel.Balance.BalanceValue += _enemyBehaviour.EnemyData.EnemyConfig.CountReward;
                _enemyBehaviour.DestroyEnemy();
            }
        }
    }
}

