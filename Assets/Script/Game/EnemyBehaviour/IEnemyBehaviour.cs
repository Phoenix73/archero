﻿using Model;
using UnityEngine;

namespace Game
{
    public interface IEnemyBehaviour
    {
        string Id { get; }
        IEnemy EnemyData { get; }

        void Init(IEnemy enemy);
        
        Transform CachedTransform { get; }

        void DestroyEnemy();
        
        IMoveEnemy MoveEnemy { get; set; }
        
        Rigidbody Rigidbody { get; }
        
        Transform TargetShoot { get; }
    }
}

