﻿using Configs;
using Purchase;

namespace Model
{
    public class GameModel
    {
        public IGameConfig GameConfig { get; }

        public IPlayer Player { get; }
        
        public IDamageManager DamageManager { get; }

        public ILevelConfig CurrentLevel { get; }
        
        public IBalance Balance { get; }
        
        private int _defaultLevel = 1;
        
        public GameModel(IGameConfig gameConfig)
        {
            GameConfig = gameConfig;
            
            Player = new Player();
            Player.PlayerConfig = gameConfig.GetPlayerConfig;
            DamageManager = new DamageManager();

            CurrentLevel = gameConfig.GetLevel(_defaultLevel);
            
            Balance = new Balance();
        }
    }
}

