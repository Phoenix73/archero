﻿using Configs;


namespace Model
{
    public interface IEnemy : IReceiveDamage
    {
        IEnemyConfig EnemyConfig { get; }
    }
}

