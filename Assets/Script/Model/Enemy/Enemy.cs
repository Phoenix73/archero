﻿using Configs;

namespace Model
{
    public class Enemy : IEnemy
    {
        public string Id { get; }
        public int HealthAmount { get; private set; }
        public void ReceiveDamage(int damage)
        {
            HealthAmount -= damage;
        }

        public IEnemyConfig EnemyConfig { get; }

        public Enemy(IEnemyConfig config, string id)
        {
            Id = id;
            EnemyConfig = config;
        }
    }
}

