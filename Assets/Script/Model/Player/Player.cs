﻿using Configs;

namespace Model
{
    public class Player : IPlayer
    {
        public string Id { get; }
        
        public void ReceiveDamage(int damage)
        {
            HealthAmount -= damage;
        }

        public int HealthAmount { get; private set; }

        private IPlayerConfig _playerConfig;

        public IPlayerConfig PlayerConfig
        {
            get => _playerConfig;
            set
            {
                _playerConfig = value;

                if (_playerConfig != null)
                {
                    HealthAmount = _playerConfig.HealthAmount;
                }
            }
        }
    }
}

