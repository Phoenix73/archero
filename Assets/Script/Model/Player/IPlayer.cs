﻿using Configs;

namespace Model
{
    public interface IPlayer : IReceiveDamage
    {
        IPlayerConfig PlayerConfig { get; set; }
    }
}

