﻿using Configs;
using Game;
using Model;
using UI;
using UnityEngine;
using View;

public class GameRoot : Singleton<GameRoot>
{
    [SerializeField] private GameConfig _gameConfig;
    [SerializeField] private GameBehaviour _gameBehaviour;
    
    private GameModel _gameModel;

    public GameModel GameModel => _gameModel;

    public GameBehaviour GameBehaviour => _gameBehaviour;

    private void Awake()
    {
        Initialize();
    }
    
    private void Initialize()
    {
        _gameModel = new GameModel(_gameConfig);
        _gameBehaviour.CreateLevel(_gameModel);

        NavigationView.Instance.PushView<StartTimerView>();
    }

    public void StartGame()
    {
        GameReport.CurrentState = GameReport.GameState.Playing;
        NavigationView.Instance.PushView<GameView>();
    }
}
