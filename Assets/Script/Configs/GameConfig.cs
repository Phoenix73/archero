﻿using System.Collections.Generic;
using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "Archero/Create GameConfig")]
    public class GameConfig : ScriptableObject, IGameConfig
    {
        [Header("Available levels")]
        [SerializeField] private List<LevelConfig> _availableLevels;

        [Header("Player")] [SerializeField] private PlayerConfig _playerConfig;
        
        public ILevelConfig GetLevel(int number)
        {
          return  _availableLevels.Find(level => level.Number == number);
        }

        public IPlayerConfig GetPlayerConfig => _playerConfig;
    }
}

