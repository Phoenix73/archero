﻿using UnityEngine;

namespace Configs
{
    public interface IEnemyConfig
    {
        int HealthAmount { get; }
        float Speed { get; }
        float MaxDistanceFromSpawn { get; }
        float TimeFreeze { get; }
        
        int DamageFromCollision { get; }
        
        IWeaponConfig WeaponConfig { get; }
        
        GameObject EnemyPrefab { get; }
        
        int CountReward { get; }
    }
}

