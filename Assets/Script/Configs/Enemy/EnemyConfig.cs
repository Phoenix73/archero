﻿using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "EnemyConfig", menuName = "Archero/Create EnemyConfig")]
    public class EnemyConfig : ScriptableObject, IEnemyConfig
    {
        [Header("Base parameters")]
        [SerializeField] private int _healthAmountAmount;
        
        [SerializeField] private float _speed;
        [SerializeField] private float _maxDistanceFromSpawn;
        
        [SerializeField] private float _timeFreeze;

        [Header("Weapon")] 
        [SerializeField] private WeaponConfig _weaponConfig;
        [SerializeField] private int _damageFromCollision;

        [Header("Weapon")] 
        [SerializeField] private int _countReward;

        [Header("Prefab")] 
        [SerializeField] private string _pathToEnemyPrefab;

        public int HealthAmount => _healthAmountAmount;
        
        public float Speed => _speed;
        public float MaxDistanceFromSpawn => _maxDistanceFromSpawn;
        
        public float TimeFreeze => _timeFreeze;

        public int DamageFromCollision => _damageFromCollision;

        public IWeaponConfig WeaponConfig => _weaponConfig;

        public GameObject EnemyPrefab => Resources.Load<GameObject>(_pathToEnemyPrefab);

        public int CountReward => _countReward;
    }  
}

