﻿

namespace Configs
{
    public interface IGameConfig
    {
        ILevelConfig GetLevel(int number);
        
        IPlayerConfig GetPlayerConfig { get; }
    }
}

