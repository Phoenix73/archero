﻿using UnityEngine;

namespace Configs
{
    public interface IMapConfig
    {
        GameObject GetPrefabMap { get; }
    }
}

