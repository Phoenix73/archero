﻿using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "MapConfig", menuName = "Archero/Create MapConfig")]
    public class MapConfig : ScriptableObject, IMapConfig
    {
        [SerializeField] private string _pathToMapPrefab;

        public GameObject GetPrefabMap => Resources.Load<GameObject>(_pathToMapPrefab);
    }
}

