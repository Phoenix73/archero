﻿using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "PlayerConfig", menuName = "Archero/Create PlayerConfig")]
    public class PlayerConfig : ScriptableObject, IPlayerConfig
    {
        [SerializeField] private string _id;
        
        [Header("Base parameters")]
        [SerializeField] private float _speed;
        [SerializeField] private int _healthAmount;

        [Header("Weapon")] 
        [SerializeField] private WeaponConfig _weaponConfig;

        [Header("Prefab")] 
        [SerializeField] private string _pathToPlayerPrefab;

        public float Speed => _speed;

        public int HealthAmount => _healthAmount;

        public WeaponConfig WeaponConfig => _weaponConfig;

        public GameObject PlayerPrefab => Resources.Load<GameObject>(_pathToPlayerPrefab);

        public string Id => _id;
    }
}

