﻿using UnityEngine;

namespace Configs
{
    public interface IPlayerConfig
    {
        string Id { get; }

        float Speed { get; }

         int HealthAmount { get; }

         WeaponConfig WeaponConfig { get; }
         
         GameObject PlayerPrefab { get; }
    }
}

