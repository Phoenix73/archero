﻿

namespace Configs
{
    public interface IEnemyStorageConfig
    {
        IEnemyConfig[] GetEnemies { get; }
    }
}

