﻿using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "EnemyStorageConfig", menuName = "Archero/Create EnemyStorageConfig")]
    public class EnemyStorageConfig : ScriptableObject, IEnemyStorageConfig
    {
        [SerializeField] private EnemyConfig[] _enemiesConfigs;

        public IEnemyConfig[] GetEnemies => _enemiesConfigs;
    }
}

