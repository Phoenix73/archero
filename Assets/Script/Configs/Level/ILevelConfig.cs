﻿

namespace Configs
{
    public interface ILevelConfig
    {
        int Number { get; }

        IEnemyStorageConfig UsedEnemyStorage { get; }

        IMapConfig MapConfig { get; }
    }
}

