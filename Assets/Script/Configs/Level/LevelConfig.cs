﻿using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "LevelConfig", menuName = "Archero/Create LevelConfig")]
    public class LevelConfig : ScriptableObject, ILevelConfig
    {
        [SerializeField] private int _number = 0;

        [SerializeField] private EnemyStorageConfig _usedEnemyStorage;

        [SerializeField] private MapConfig _mapConfig;

        public int Number => _number;

        public IEnemyStorageConfig UsedEnemyStorage => _usedEnemyStorage;

        public IMapConfig MapConfig => _mapConfig;
    }
}

