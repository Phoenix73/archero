﻿using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "WeaponConfig", menuName = "Archero/Create WeaponConfig")]
    public class WeaponConfig : ScriptableObject, IWeaponConfig
    {
        [SerializeField] private int _damage;
        [SerializeField] private float _timeReload;
        [SerializeField] private float _speedShell;

        [SerializeField] private string _pathToPrefab;

        public GameObject PrefabWeapon => Resources.Load<GameObject>(_pathToPrefab);

        public int Damage => _damage;

        public float SecondsReload => _timeReload;
        public float SpeedShell => _speedShell;
    }
}

