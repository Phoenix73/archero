﻿using UnityEngine;

namespace Configs
{
    public interface IWeaponConfig
    {
        int Damage { get; }

         float SecondsReload { get; }

         float SpeedShell { get; }
         
         GameObject PrefabWeapon { get; }
    } 
}

