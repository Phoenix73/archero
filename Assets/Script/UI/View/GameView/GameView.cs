﻿using TMPro;
using UniRx;
using UnityEngine;
using View;

namespace UI
{
    public class GameView : BaseView
    {
        [SerializeField] private TextMeshProUGUI _labelBalance;
        protected override void Init()
        {
            SetBalance(GameRoot.Instance.GameModel.Balance.BalanceValue);

            GameRoot.Instance.GameModel.Balance.OnChangeBalance.Subscribe(OnChangeBalance).AddTo(this);
        }

        protected override void Dispose()
        {
            
        }

        private void OnChangeBalance(int newValue)
        {
            SetBalance(newValue);
        }

        private void SetBalance(int newValue)
        {
            _labelBalance.text = newValue.ToString();
        }
    }
}

