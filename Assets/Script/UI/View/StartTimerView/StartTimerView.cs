﻿using Game;
using UnityEngine;

namespace View
{
    public class StartTimerView : BaseView
    {
        [SerializeField] private StartTimerContent _startTimerContent;
        
        protected override void Init()
        {
            _startTimerContent.StartCountDown();
        }

        protected override void Dispose()
        {
            _startTimerContent.Dispose();
        }
    }
}

