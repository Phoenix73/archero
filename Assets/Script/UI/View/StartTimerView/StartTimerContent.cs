﻿using System;
using TMPro;
using UniRx;
using UnityEngine;
using Utils;
using View;

namespace Game
{
    public class StartTimerContent : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _labelTimer;
        
        private IDisposable _timerSubscribe;

        private float _startValueCountdown = 3;
        private float _endValueCountdown = 0;
        
        private float _timeUpdate = 1;

        private float _currentValueCountdown = 0;
        private bool _isCountDownActive = false;
        
        public void StartCountDown()
        {
            _isCountDownActive = true;
            _currentValueCountdown = _startValueCountdown;
            _timerSubscribe = Observable.Interval(TimeSpan.FromSeconds(_timeUpdate)).Where(_ => _isCountDownActive).Subscribe(_ => UpdateTimer());
        }

        private void UpdateTimer()
        {
            _currentValueCountdown -= _timeUpdate;
            _currentValueCountdown = ClampUtils.Clamp(_currentValueCountdown, _startValueCountdown, _endValueCountdown);
            _labelTimer.text = _currentValueCountdown.ToString();

            if (_currentValueCountdown <= _endValueCountdown)
            {
                OnFinishedCountdown();
            }
        }

        private void OnFinishedCountdown()
        {
            _isCountDownActive = false;
            NavigationView.Instance.PopView();
            GameRoot.Instance.StartGame();
        }

        public void Dispose()
        {
            _timerSubscribe?.Dispose();
        }
    }
}

