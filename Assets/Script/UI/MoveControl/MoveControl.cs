﻿using UnityEngine;

namespace UI
{
    public class MoveControl : MonoBehaviour, IMoveControl
    {
        private Vector3 _currentDirection = Vector3.zero;
        private int _valueVectorDirection = 1;
        
        private void Update()
        {
            _currentDirection = Vector3.zero;
            
            ClickRightMove();
            ClickLeftMove();
            ClickUpMove();
            ClickDownMove();
        }

        private void ClickRightMove()
        {
            if (Input.GetKey(KeyCode.D))
            {
                _currentDirection.x = _valueVectorDirection;
            }
        }
        
        private void ClickLeftMove()
        {
            if (Input.GetKey(KeyCode.A))
            {
                _currentDirection.x = -_valueVectorDirection;
            }
        }
        
        private void ClickUpMove()
        {
            if (Input.GetKey(KeyCode.W))
            {
                _currentDirection.z = _valueVectorDirection;
            }
        }
        
        private void ClickDownMove()
        {
            if (Input.GetKey(KeyCode.S))
            {
                _currentDirection.z = -_valueVectorDirection;
            }
        }

        public Vector3 GetDirection => _currentDirection;
    }
}

