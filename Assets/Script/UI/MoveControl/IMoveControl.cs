﻿using UnityEngine;

namespace UI
{
    public interface IMoveControl
    {
        Vector3 GetDirection { get; }
    }
}

