﻿

namespace Utils
{
    public static class TagStorage
    {
        public const string TagEnemy = "Enemy";
        public const string TagPlayer = "Player";
        public const string TagShell = "Shell";
        public const string TagWall = "Wall";
        public const string TagWin = "Win";
    }
}

