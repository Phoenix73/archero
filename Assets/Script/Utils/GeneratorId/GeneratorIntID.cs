﻿
namespace Utils
{
    public class GeneratorIntID : IGeneratorIntID
    {
        private int _lastId = 0;

        public int GetId
        {
            get
            {
                int returnedId = _lastId;
                
                IncreaseId();
                return returnedId;
            }
        }

        private void IncreaseId()
        {
            _lastId++;
            if (_lastId == int.MaxValue)
            {
                _lastId = 0;
            }
        }
    }
}

